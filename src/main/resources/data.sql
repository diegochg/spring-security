CREATE TABLE Products(id bigint PRIMARY KEY, name varchar(30) NOT NULL, price float NOT NULL);
CREATE SEQUENCE sq_product_id_pk INCREMENT BY 1 MINVALUE 1 MAXVALUE 9999999999 START 1 NO CYCLE;
ALTER SEQUENCE sq_product_id_pk OWNED BY Products.id;
-------------------------------------------------------------
CREATE TABLE Users(id bigint PRIMARY KEY, name varchar(40) NOT NULL, userName varchar(30) UNIQUE NOT NULL,
	password varchar(80) NOT NULL);
CREATE SEQUENCE sq_user_id_pk INCREMENT BY 1 MINVALUE 1 MAXVALUE 9999999999 START 1 NO CYCLE;
ALTER SEQUENCE sq_user_id_pk OWNED BY Users.id;
-------------------------------------------------------------
CREATE TABLE Roles(id bigint PRIMARY KEY, name varchar(50) UNIQUE NOT NULL);
CREATE SEQUENCE sq_role_id_pk INCREMENT BY 1 MINVALUE 1 MAXVALUE 9999999999 START 1 NO CYCLE;
ALTER SEQUENCE sq_role_id_pk OWNED BY Roles.id;
-------------------------------------------------------------
CREATE TABLE Users_Roles(user_id bigint references Users(id), role_id bigint references Roles(id),
	PRIMARY KEY(user_id,role_id));
-------------------------------------------------------------
INSERT INTO Roles VALUES(1,'ROLE_USER');
INSERT INTO Roles VALUES(2,'ROLE_ADMIN');
INSERT INTO Roles VALUES(3,'ROLE_SUPER_ADMIN');