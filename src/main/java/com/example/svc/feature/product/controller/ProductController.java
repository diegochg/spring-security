package com.example.svc.feature.product.controller;

import com.example.svc.feature.product.dto.ProductDto;
import com.example.svc.feature.product.model.Product;
import com.example.svc.feature.product.service.ProductService;
import com.example.svc.util.Message;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
//@CrossOrigin("http://localhost:4200")
public class ProductController {

  @Autowired
  private ProductService productService;

  @GetMapping("/all")
  public ResponseEntity<List<Product>> getAllProducts() {
    return new ResponseEntity(productService.getAllProducts(), HttpStatus.OK);
  }

  @GetMapping("/detail/{id}")
  public ResponseEntity<Product> getProductById(@PathVariable("id") Long id) {
    return new ResponseEntity(productService.getOneProductById(id), HttpStatus.OK);
  }

  @PostMapping("/create")
  @PreAuthorize("hasRole('ADMIN')")
  public ResponseEntity<Message> createProduct(@RequestBody @Valid ProductDto productDto) {
    productService.saveProduct(productDto);
    return new ResponseEntity(new Message("Product created"), HttpStatus.CREATED);
  }

  @PutMapping("/update/{id}")
  @PreAuthorize("hasRole('SUPER_ADMIN')")
  public ResponseEntity<Message> updateProduct(@PathVariable("id") Long id,
    @RequestBody @Valid ProductDto productDto) {
    productService.updateProduct(id, productDto);
    return new ResponseEntity(new Message("Product updated"), HttpStatus.OK);
  }

  @DeleteMapping("/delete/{id}")
  @PreAuthorize("hasRole('SUPER_ADMIN')")
  public ResponseEntity<Message> deleteProduct(@PathVariable("id") Long id) {
    productService.deleteProduct(id);
    return new ResponseEntity<>(new Message("Product deleted"), HttpStatus.OK);
  }

}
