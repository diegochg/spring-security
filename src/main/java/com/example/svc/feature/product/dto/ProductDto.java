package com.example.svc.feature.product.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductDto {

  @NotBlank(message = "Name is required")
  private String name;

  @NotNull(message = "Price is required")
  @Min(0)
  private float price;

}
