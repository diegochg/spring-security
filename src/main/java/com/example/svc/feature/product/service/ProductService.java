package com.example.svc.feature.product.service;

import com.example.svc.feature.product.dto.ProductDto;
import com.example.svc.feature.product.model.Product;
import java.util.List;

public interface ProductService {

  List<Product> getAllProducts();

  Product getOneProductById(Long id);

  Product getOneProductByName(String name);

  void saveProduct(ProductDto productDto);

  void updateProduct(Long id, ProductDto productDto);

  void deleteProduct(Long id);

}
