package com.example.svc.feature.product.service;

import com.example.svc.error.ApiErrorCode;
import com.example.svc.error.ApiException;
import com.example.svc.feature.product.dto.ProductDto;
import com.example.svc.feature.product.model.Product;
import com.example.svc.feature.product.repository.ProductRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

  @Autowired
  private ProductRepository productRepository;

  @Override
  public List<Product> getAllProducts() {
    return productRepository.findAll();
  }

  @Override
  public Product getOneProductById(Long id) {
    return productRepository.findById(id).orElseThrow(() ->
      new ApiException(HttpStatus.BAD_REQUEST, ApiErrorCode.PRODUCT_DOEST_NOT_EXISTS,
        "Product does not exist"));
  }

  @Override
  public Product getOneProductByName(String name) {
    return productRepository.findByName(name).orElseThrow(() ->
      new ApiException(HttpStatus.BAD_REQUEST, ApiErrorCode.PRODUCT_DOEST_NOT_EXISTS,
        "Product does not exist"));
  }

  @Override
  public void saveProduct(ProductDto productDto) {
    if (existsProductByName(productDto.getName())) {
      throw new ApiException(HttpStatus.BAD_REQUEST, ApiErrorCode.PRODUCT_NAME_EXISTS,
        "Product name already exists");
    }
    productRepository.save(new Product(productDto.getName(), productDto.getPrice()));
  }

  @Override
  public void updateProduct(Long idProductToUpdate, ProductDto productDto) {
    if (!existsProductById(idProductToUpdate)) {
      throw new ApiException(HttpStatus.BAD_REQUEST, ApiErrorCode.PRODUCT_DOEST_NOT_EXISTS,
        "Product does not exist");
    }
    if (existsProductByName(productDto.getName())
      && getOneProductByName(productDto.getName()).getId() != idProductToUpdate) {
      throw new ApiException(HttpStatus.BAD_REQUEST, ApiErrorCode.PRODUCT_NAME_EXISTS,
        "Product name already exists");
    }
    Product productToUpdate = getOneProductById(idProductToUpdate);
    productToUpdate.setName(productDto.getName());
    productToUpdate.setPrice(productDto.getPrice());
    productRepository.save(productToUpdate);
  }

  @Override
  public void deleteProduct(Long id) {
    if (!existsProductById(id)) {
      throw new ApiException(HttpStatus.BAD_REQUEST, ApiErrorCode.PRODUCT_DOEST_NOT_EXISTS,
        "Product does not exist");
    }
    productRepository.deleteById(id);
  }

  public boolean existsProductById(Long id) {
    return productRepository.existsById(id);
  }

  public boolean existsProductByName(String name) {
    return productRepository.existsByName(name);
  }

}
