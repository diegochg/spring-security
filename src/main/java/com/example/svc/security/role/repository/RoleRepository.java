package com.example.svc.security.role.repository;

import com.example.svc.security.role.enums.RoleName;
import com.example.svc.security.role.model.Role;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

  Optional<Role> findByName(RoleName roleName);

}
