package com.example.svc.security.role.enums;

public enum RoleName {

  ROLE_SUPER_ADMIN, ROLE_ADMIN, ROLE_USER

}
