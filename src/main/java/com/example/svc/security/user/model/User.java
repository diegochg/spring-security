package com.example.svc.security.user.model;

import com.example.svc.security.role.model.Role;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "Users")
@NoArgsConstructor
public class User implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ")
  @SequenceGenerator(sequenceName = "sq_user_id_pk", allocationSize = 1, name = "SEQ")
  private Long id;

  private String name;

  private String userName;

  private String password;

  @ManyToMany/*(fetch = FetchType.LAZY)*/
  @JoinTable(name = "Users_Roles",
    joinColumns = @JoinColumn(name = "user_id"),
    inverseJoinColumns = @JoinColumn(name = "role_id"))
  private Set<Role> roles = new HashSet<>();

  public User(@NotNull String name, @NotNull String userName, @NotNull String password) {
    this.name = name;
    this.userName = userName;
    this.password = password;
  }

}
