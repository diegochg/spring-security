package com.example.svc.security.user.service;

import com.example.svc.error.ApiErrorCode;
import com.example.svc.error.ApiException;
import com.example.svc.security.user.model.User;
import com.example.svc.security.user.model.PrimaryUser;
import com.example.svc.security.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userRepository.findByUserName(username).orElseThrow(() -> new ApiException(
      HttpStatus.BAD_REQUEST, ApiErrorCode.USER_NAME_NOT_EXISTS,
      "UserName does not exist"));
    return PrimaryUser.build(user);
  }

}
