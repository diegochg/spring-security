package com.example.svc.security.jwt;

import com.example.svc.security.user.model.PrimaryUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class JwtProvider {

  @Value("${jwt.secret}")
  private String secret;

  @Value("${jwt.expiration}")
  private Integer expiration;

  public String extractUserName(String token) {
    return extractAllClaims(token).getSubject();
  }

  public Date extractExpiration(String token) {
    return extractAllClaims(token).getExpiration();
  }

  public Claims extractAllClaims(String token) {
    return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
  }

  public String generateToken(PrimaryUser primaryUser) {
    Map<String, Object> claims = new HashMap<>();
    //claims.put(USER_CODE, userAccountDetails.getUserCode());
    return createToken(claims, primaryUser.getUsername());
  }

  public String createToken(Map<String, Object> claims, String subject) {
    return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date())
      .setExpiration(new Date(new Date().getTime() + expiration))
      .signWith(SignatureAlgorithm.HS512, secret).compact();
  }

  public boolean validateToken(String token) {
    try {
      Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
      return true;
    } catch (MalformedJwtException malformedJwtException) {
      log.error("Token malformed");
    } catch (UnsupportedJwtException unsupportedJwtException) {
      log.error("Token not supported");
    } catch (ExpiredJwtException expiredJwtException) {
      log.error("Token expired");
    } catch (IllegalArgumentException illegalArgumentException) {
      log.error("Token empty");
    } catch (SignatureException signatureException) {
      log.error("Signature failure");
    }
    return false;
  }

  public String getTokenFromHeader(HttpServletRequest httpServletRequest) {
    String token = null;
    String header = httpServletRequest.getHeader("Authorization");
    if (header != null && header.startsWith("Bearer")) {
      token = header.replace("Bearer ", "");
    }
    return token;
  }

}
