package com.example.svc.security.authentication.service;

import com.example.svc.error.ApiErrorCode;
import com.example.svc.error.ApiException;
import com.example.svc.security.authentication.dto.LoginUser;
import com.example.svc.security.authentication.dto.NewUser;
import com.example.svc.security.role.enums.RoleName;
import com.example.svc.security.role.model.Role;
import com.example.svc.security.role.repository.RoleRepository;
import com.example.svc.security.user.model.User;
import com.example.svc.security.user.repository.UserRepository;
import com.example.svc.security.jwt.JwtProvider;
import com.example.svc.security.user.model.PrimaryUser;
import com.example.svc.security.user.service.UserDetailsServiceImpl;
import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private UserDetailsServiceImpl userDetailsServiceImpl;

  @Autowired
  private JwtProvider jwtProvider;


  @Override
  public void createUser(NewUser newUser, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ApiException(HttpStatus.BAD_REQUEST, ApiErrorCode.VALIDATION_FAILED,
        "Wrong fields");
    }
    if (userRepository.existsByUserName(newUser.getUserName())) {
      throw new ApiException(HttpStatus.BAD_REQUEST, ApiErrorCode.USER_NAME_EXISTS,
        "UserName already exists");
    }
    User user = new User(newUser.getName(), newUser.getUserName(),
      newUser.getPassword());
    Set<Role> roles = new HashSet<>();
    roles.add(roleRepository.findByName(RoleName.ROLE_USER).get());
    if (newUser.getRoles() != null && newUser.getRoles().stream()
      .allMatch(x -> x.equals("ADMIN"))) {
      roles.add(roleRepository.findByName(RoleName.ROLE_ADMIN).get());
    }
    if (newUser.getRoles() != null && newUser.getRoles().stream()
      .allMatch(x -> x.equals("SUPER_ADMIN"))) {
      roles.add(roleRepository.findByName(RoleName.ROLE_ADMIN).get());
      roles.add(roleRepository.findByName(RoleName.ROLE_SUPER_ADMIN).get());
    }
    user.setRoles(roles);
    userRepository.save(user);
  }

  @Override
  public String getTokenForUser(LoginUser loginUser) {
    if (userRepository.existsByUserName(loginUser.getUserName())) {
      Authentication authentication = authenticationManager.authenticate
        (new UsernamePasswordAuthenticationToken(loginUser.getUserName(), loginUser.getPassword()));
      SecurityContextHolder.getContext().setAuthentication(authentication);
      PrimaryUser primaryUser = (PrimaryUser) userDetailsServiceImpl
        .loadUserByUsername(loginUser.getUserName());
      String tokenForUser = jwtProvider.generateToken(primaryUser);
      return tokenForUser;
    }
    return null;
  }

}
