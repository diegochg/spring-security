package com.example.svc.security.authentication.service;

import com.example.svc.security.authentication.dto.LoginUser;
import com.example.svc.security.authentication.dto.NewUser;
import org.springframework.validation.BindingResult;

public interface AuthenticationService {

  void createUser(NewUser newUser, BindingResult bindingResult);

  String getTokenForUser(LoginUser loginUser);

}
