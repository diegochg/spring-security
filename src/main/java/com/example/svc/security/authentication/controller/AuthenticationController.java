package com.example.svc.security.authentication.controller;

import static org.springframework.http.HttpStatus.OK;

import com.example.svc.error.ApiErrorCode;
import com.example.svc.error.ApiException;
import com.example.svc.security.authentication.dto.LoginUser;
import com.example.svc.security.authentication.dto.NewUser;
import com.example.svc.security.authentication.dto.Token;
import com.example.svc.security.authentication.service.AuthenticationService;
import com.example.svc.security.role.repository.RoleRepository;
import com.example.svc.security.jwt.JwtProvider;
import com.example.svc.util.Message;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@CrossOrigin
public class AuthenticationController {

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private RoleRepository roleRepository;

  @Autowired
  private JwtProvider jwtProvider;

  @Autowired
  private AuthenticationService authenticationService;

  @PostMapping("/new")
  public ResponseEntity<?> newUser(@Valid @RequestBody NewUser newUser,
    BindingResult bindingResult) {
    authenticationService.createUser(newUser, bindingResult);
    return new ResponseEntity(new Message("User created"), HttpStatus.CREATED);
  }

  @PostMapping("/login")
  public ResponseEntity<String> login(@Valid @RequestBody LoginUser loginUser,
    BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ApiException(HttpStatus.BAD_REQUEST, ApiErrorCode.VALIDATION_FAILED,
        "Wrong fields");
    }
    return new ResponseEntity(new Token(authenticationService.getTokenForUser(loginUser)), OK);
  }

}
