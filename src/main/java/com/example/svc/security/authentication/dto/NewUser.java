package com.example.svc.security.authentication.dto;

import com.example.svc.security.role.enums.RoleName;
import java.util.Set;
import javax.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NewUser {

  @NotBlank
  private String name;

  @NotBlank
  private String userName;

  @NotBlank
  private String password;

  private Set<String> roles;

}
