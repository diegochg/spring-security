package com.example.svc.security.authentication.dto;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;

public class JwtDtoResponse {

  private String token;
  private String bearer = "Bearer";
  private String userName;
  private Collection<? extends GrantedAuthority> authorities;

  public JwtDtoResponse(String token, String userName,
    Collection<? extends GrantedAuthority> authorities) {
    this.token = token;
    this.userName = userName;
    this.authorities = authorities;
  }

}
