package com.example.svc.error;

public class ApiErrorCode {

  public static final String VALIDATION_FAILED = "VALIDATION_ERROR";

  public static final String PRODUCT_DOEST_NOT_EXISTS = "PRODUCT_ERROR_01";
  public static final String PRODUCT_NAME_EXISTS = "PRODUCT_ERROR_02";

  public static final String USER_NAME_NOT_EXISTS = "USER_ERROR_01";
  public static final String USER_NAME_EXISTS = "USER_ERROR_02";

}
