package com.example.svc.error;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

@Getter
@Setter
public class ApiException extends ResponseStatusException {

  private String apiCode;

  public ApiException(HttpStatus status, String apiCode, String reason) {
    super(status, reason);
    this.apiCode = apiCode;
  }

}
