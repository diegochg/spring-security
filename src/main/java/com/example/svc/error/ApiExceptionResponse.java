package com.example.svc.error;

import java.time.LocalDateTime;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiExceptionResponse {

  private LocalDateTime timestamp;
  private String message;
  private String path;
  private String apiCode;
  private List<String> details;

}
