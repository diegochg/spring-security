package com.example.svc.error;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class ApiErrorHandler extends ResponseEntityExceptionHandler {

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
    HttpHeaders headers, HttpStatus status, WebRequest request) {
    List<String> details = new ArrayList<>();
    for (FieldError error : ex.getBindingResult().getFieldErrors()) {
      details.add(error.getField() + " : " + error.getDefaultMessage());
    }
    ApiExceptionResponse errorResponseDetail = new ApiExceptionResponse();
    errorResponseDetail.setTimestamp(LocalDateTime.now());
    errorResponseDetail.setMessage("Validation Failed");
    errorResponseDetail.setPath(request.getDescription(false));
    errorResponseDetail.setApiCode(ApiErrorCode.VALIDATION_FAILED);
    errorResponseDetail.setDetails(details);
    return handleExceptionInternal(ex, errorResponseDetail, headers, HttpStatus.BAD_REQUEST,
      request);
  }

  @ExceptionHandler(ApiException.class)
  public final ResponseEntity<ApiExceptionResponse> errorHandler(ApiException apiException,
    WebRequest webRequest) {
    ApiExceptionResponse apiExceptionResponse = new ApiResponseBuilder(apiException, webRequest)
      .build();
    return new ResponseEntity(apiExceptionResponse, apiException.getStatus());
  }

  private static class ApiResponseBuilder {

    private ApiException apiException;
    private WebRequest webRequest;

    public ApiResponseBuilder(ApiException apiException,
      WebRequest webRequest) {
      this.apiException = apiException;
      this.webRequest = webRequest;
    }

    public ApiExceptionResponse build() {
      ApiExceptionResponse apiExceptionResponse = new ApiExceptionResponse();
      apiExceptionResponse.setTimestamp(LocalDateTime.now());
      String reason = this.apiException.getReason();
      String apiCode = this.apiException.getApiCode();
      if (StringUtils.isEmpty(reason)) {
        reason = this.apiException.getStatus().getReasonPhrase();
      }
      if (StringUtils.isEmpty(apiCode)) {
        apiCode = "" + this.apiException.getStatus().value();
      }
      apiExceptionResponse.setMessage(reason);
      apiExceptionResponse.setPath(this.webRequest.getDescription(false));
      apiExceptionResponse.setApiCode(apiCode);
      return apiExceptionResponse;
    }

  }
}
